from sqlalchemy import Column, ForeignKey, Integer, String
from app.models.usersModel import Users
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask import request


engine = create_engine('mysql+mysqlconnector://root:motherrussia@localhost:3306/employee_training', echo = True)
Session = sessionmaker(bind=engine)
Base = declarative_base()

class Form_User(Base):
    __tablename__ = 'form_user_register'
    id = Column(Integer,primary_key=True)
    user_id = Column(Integer, ForeignKey(Users.id))
    address = Column(String)
    division = Column(String)
    position = Column(String)
    year_submission = Column(Integer)

    def __init__(self,user_id:int,address:str,division:str,position:str,year_submission:int):
        self.user_id = user_id
        self.address = address
        self.division = division
        self.position = position
        self.year_submission = year_submission