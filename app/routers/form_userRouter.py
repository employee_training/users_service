from app import app
from app.controllers import form_userController
from flask import Blueprint, request

form_user_blueprint = Blueprint("form_userRouter",__name__)

@app.route("/form_user_combined/",methods=["GET"])
def showJoinUserForm():
    return form_userController.showJoinUserForm()

@app.route("/form_user",methods=["GET"])
def showFormUser():
    return form_userController.showFormUser()

@app.route("/form_user/<int:userid>",methods=["GET"])
def showFormUserbyUserId(userid):
    return form_userController.showFormUserbyUserId(userid)

@app.route("/form_user/create",methods=["POST"])
def insertFormUser():
    params = request.json
    return form_userController.insertFormUser(**params)

@app.route("/form_user/update/<int:userid>",methods=["POST"])
def updateFormUserById(userid):
    params = request.json
    return form_userController.updateFormUserById(userid,**params)

@app.route('/form_user/delete/<int:userid>',methods=['DELETE'])
def deleteFormUserById(userid):
    params = request.json
    return form_userController.deleteFormUserById(userid,**params)

